package com.example.cswtest.api;

import com.example.cswtest.model.Product;
import com.example.cswtest.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/products")
@Slf4j
@RequiredArgsConstructor
public class ProductAPI {
    private final ProductService productService;

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts() {
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @PostMapping
    public ResponseEntity addProduct(@Valid @RequestBody Product p) {
        return ResponseEntity.ok(productService.saveProduct(p));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> sellProduct(@PathVariable Long id, @PathVariable Integer quantity) {
        Optional<Product> p = productService.findById(id);
        if(!p.isPresent()){
            log.error("Product not exist");
            ResponseEntity.notFound().build();
        }
        if(p.get().getQuantity() < quantity) {
            log.error("Quantity not enough");
            ResponseEntity.badRequest().build();
        }
        p.get().setId(id);
        p.get().setQuantity(p.get().getQuantity() - quantity);
        return ResponseEntity.ok(productService.saveProduct(p.get()));
    }

}
