package com.example.cswtest.service;

import com.example.cswtest.model.Product;
import com.example.cswtest.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product saveProduct(Product p) {
        return productRepository.save(p);
    }

    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }
}
